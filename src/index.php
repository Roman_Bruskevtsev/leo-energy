<?php
/**
 *
 * @package WordPress
 * @subpackage Leo-energy
 * @since 1.0
 * @version 1.0
 */
get_header(); 

get_template_part( 'template-parts/page/banner' );
get_template_part( 'template-parts/page/breadcrumb' );

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'home_banner' ): 
            get_template_part( 'inc/acf-content/home_banner' );
        elseif( get_row_layout() == 'about_us_section' ): 
            get_template_part( 'inc/acf-content/about_us_section' );
        elseif( get_row_layout() == 'service_section' ): 
            get_template_part( 'inc/acf-content/service_section' );
        elseif( get_row_layout() == 'contact_form_section' ): 
            get_template_part( 'inc/acf-content/contact_form_section' );
        elseif( get_row_layout() == 'objects_section' ): 
            get_template_part( 'inc/acf-content/objects_section' );
        elseif( get_row_layout() == 'contact_section' ): 
            get_template_part( 'inc/acf-content/contact_section' );
        elseif( get_row_layout() == 'about_company_section' ): 
            get_template_part( 'inc/acf-content/about_company_section' );
        elseif( get_row_layout() == 'partners_section' ): 
            get_template_part( 'inc/acf-content/partners_section' );
        endif;
    endwhile;
endif;

get_footer();