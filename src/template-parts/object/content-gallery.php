<?php
$gallery = get_field('gallery');
?>
<section class="object__gallery">
	<div class="container">
		<div class="row">
			<div class="col">
				<h3 data-aos="fade-left" data-aos-duration="1000"><?php the_title(); ?></h3>
			</div>
		</div>
		<?php if($gallery) { ?>
		<div class="row">
		<?php foreach ($gallery as $image) { ?>
			<div class="col-md-6 col-lg-4">
				<a class="object__image" data-aos="fade-up" data-aos-duration="1000" href="<?php echo $image['image']['url']; ?>" data-sub-html="<?php echo $image['text']; ?>">
					<div class="thumbnail">
						<img src="<?php echo $image['image']['sizes']['object-small']; ?>" alt="<?php echo $image['image']['title']; ?>">
					</div>
				</a>
			</div>
		<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>
