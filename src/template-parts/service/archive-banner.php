<?php 
$background = get_field('services_banner', 'option')['banner'] ? ' style="background-image: url('.get_field('services_banner', 'option')['banner']['url'].')"' : '';
?>
<section class="objects__banner"<?php echo $background; ?>>
	<div class="content">
		<div class="container">
			<?php if( get_field('services_banner', 'option')['title'] ) { ?>
			<div class="row">
				<div class="col-lg-12">
					<h1 data-aos="fade-left" data-aos-duration="1000"><?php echo get_field('services_banner', 'option')['title']; ?></h1>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>