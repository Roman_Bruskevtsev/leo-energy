<section class="service__information">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<?php if( get_field('image') ) { ?>
				<div class="image">
					<img src="<?php echo get_field('image')['url'] ?>" alt="<?php echo get_field('image')['title'] ?>">
				</div>
				<?php } ?>
			</div>
			<div class="col-lg-6">
				<?php if( get_field('text') ) { ?>
				<div class="text">
					<?php the_field('text'); ?>
				</div>
				<?php } 
				if( get_field('file_url') ) { ?>
					<a class="file" href="<?php the_field('file_url'); ?>"><?php the_field('file_name'); ?></a>
				<?php } ?>
			</div>
		</div>
	</div>
</section>