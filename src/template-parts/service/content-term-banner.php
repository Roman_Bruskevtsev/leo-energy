<?php 
$term_info = get_queried_object();
$term_tax = $term_info->taxonomy;
$term_id = $term_info->term_id;

$term = get_term($term_id, 'service-category');

$background = ( get_field( 'thumbnail', 'service-category_'.$term_id ) ) ? ' style="background-image: url('.get_field( 'thumbnail', 'service-category_'.$term_id ).');"' : '';
?>
<section class="page__banner"<?php echo $background; ?>>
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="title" data-aos="fade-left" data-aos-duration="1000">
						<h1><b><?php echo $term->name; ?></b></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>