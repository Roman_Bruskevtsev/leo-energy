<?php
$background = ( get_the_post_thumbnail_url(get_the_ID(), 'full') ) ? ' style="background-image: url('.get_the_post_thumbnail_url(get_the_ID(), 'full').');"' : '';
?>
<section class="page__banner"<?php echo $background; ?>>
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="title">
						<h1 data-aos="fade-left" data-aos-duration="1000"><b><?php the_title(); ?></b></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>