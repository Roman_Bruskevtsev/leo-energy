<?php
/**
 * Template Name: coming-soon
 *
 * @package WordPress
 * @subpackage Elektrotehnika
 * @since 1.0
 * @version 1.0
 */
get_header(); 

$background = ( get_field('background_image') ) ? ' style="background-image: url('.get_field('background_image').');"' : '';

?>

<section<?php echo $background; ?> class="full__screen">
	<div class="coming__soon text-center">
		<?php if( get_field('title') ){ ?>
			<h1><?php the_field('title'); ?></h1>
		<?php } 
		if( get_field('form_shortcode') ) { ?>
			<div class="form__inline">
				<?php echo do_shortcode( get_field('form_shortcode') ); ?>
			</div>
		<?php } ?>
	</div>
	<?php if( get_field('logo') ) { 
		$logo = get_field('logo');
	?>
	<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
		<img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
	</a>
	<?php } ?>
</section>

<?php get_footer();