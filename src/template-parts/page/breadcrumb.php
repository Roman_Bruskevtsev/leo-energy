<section class="breadcrumb__section" data-aos="fade-up" data-aos-duration="1000">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
			<?php if( function_exists('bcn_display') ) { ?>
				<div class="breadcrumb"><?php bcn_display(); ?></div>
			<?php } ?>
			</div>
		</div>
	</div>
</section>