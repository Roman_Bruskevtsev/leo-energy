<?php
/**
 *
 * @package WordPress
 * @subpackage Leo-energy
 * @since 1.0
 */

/*ACF Import*/
require get_template_directory() . '/inc/acf-import.php';

/*Elektrotehnika*/
require get_template_directory() . '/inc/classes/elektrotehnika.php';