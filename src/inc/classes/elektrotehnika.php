<?php
/**
 * 
 */
class Elektrotehnika {
	var $version    = '1.0.1';

	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/assets/js';
        $this->stylesDir = get_theme_file_uri().'/assets/css';
        $this->classDir = get_theme_file_uri().'/inc/classes/assets';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts_styles' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
		add_action( 'wp_footer',  array( $this, 'js_variables' ) );
		add_action( 'init', array( $this, 'custom_posts_type') );
		add_action( 'init', array( $this, 'custom_taxonomy') );

		add_filter( 'upload_mimes', array( $this, 'enable_svg_types'), 99 );

		add_action( 'wp_ajax_load_objects', array( $this, 'load_objects' ) );
        add_action( 'wp_ajax_nopriv_load_objects', array( $this, 'load_objects' ) );
	}

	public function scripts_styles() {
		wp_enqueue_style( 'elektrotehnika-css', $this->stylesDir.'/main.min.css' , '', $this->version);
    	wp_enqueue_style( 'elektrotehnika-style', get_stylesheet_uri() );

    	wp_enqueue_script( 'mousewheel-smoothscroll-js', $this->scriptsDir.'/mousewheel-smoothscroll.min.js', array( 'jquery' ), $this->version, true );
    	wp_enqueue_script( 'slick-js', $this->scriptsDir.'/slick.min.js', array( 'jquery' ), $this->version, true );
    	wp_enqueue_script( 'aos-js', $this->scriptsDir.'/aos.min.js', array( 'jquery' ), $this->version, true );
    	wp_enqueue_script( 'lightgallery-js', $this->scriptsDir.'/lightgallery.min.js', array( 'jquery' ), $this->version, true );
    	wp_enqueue_script( 'elektrotehnika-js', $this->classDir.'/js/elektrotehnika.min.js', array( 'jquery' ), $this->version, true );
        wp_enqueue_script( 'script-js', $this->scriptsDir.'/main.min.js', array( 'jquery' ), $this->version, true );
        
    }

    public function theme_setup(){
    	load_theme_textdomain( 'elektrotehnika' );
	    add_theme_support( 'automatic-feed-links' );
	    add_theme_support( 'title-tag' );
	    add_theme_support( 'post-thumbnails' );

	    add_image_size( 'service-thumbnail', 262, 270, true );
	    add_image_size( 'object-thumbnail', 504, 320, true );
	    add_image_size( 'object-small', 330, 290, true );

	    register_nav_menus( array(
	        'main'          => __( 'Main Menu', 'elektrotehnika' ),
	        'footer'        => __( 'Footer Menu', 'elektrotehnika' )
	    ) );

	    if( function_exists('acf_add_options_page') ) {
		    $general = acf_add_options_page(array(
		        'page_title'    => 'Theme General Settings',
		        'menu_title'    => 'Theme Settings',
		        'redirect'      => false,
		        'capability'    => 'edit_posts',
		        'menu_slug'     => 'theme-settings',
		    ));
		}
    }

    public function enable_svg_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	public function js_variables(){ ?>
		<script type="text/javascript">
	        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	    </script>
	<?php }

	public function custom_posts_type(){
		$objects_labels = array(
			'name'					=> __('Objects', 'elektrotehnika'),
			'singular_name'			=> __('Object', 'elektrotehnika'),
			'add_new'				=> __('Add Object', 'elektrotehnika'),
			'add_new_item'			=> __('Add New Object', 'elektrotehnika'),
			'edit_item'				=> __('Edit Object', 'elektrotehnika'),
			'new_item'				=> __('New Object', 'elektrotehnika'),
			'view_item'				=> __('View Object', 'elektrotehnika')
		);

		$objects_args = array(
			'label'               => __('Objects', 'elektrotehnika'),
			'description'         => __('Object information page', 'elektrotehnika'),
			'labels'              => $objects_labels,
			'supports'            => array( 'title', 'excerpt', 'thumbnail'),
			'taxonomies'          => array( 'objects-categories' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'has_archive'         => true,
			'can_export'          => true,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'query_var'           => true,
			'rewrite'             => '',
			'capability_type'     => 'post',
			'menu_position'		  => 4,
			'menu_icon'           => 'dashicons-share-alt'
		);
		register_post_type( 'object', $objects_args );

		$services_labels = array(
			'name'					=> __('Services', 'elektrotehnika'),
			'singular_name'			=> __('Service', 'elektrotehnika'),
			'add_new'				=> __('Add Service', 'elektrotehnika'),
			'add_new_item'			=> __('Add New Service', 'elektrotehnika'),
			'edit_item'				=> __('Edit Service', 'elektrotehnika'),
			'new_item'				=> __('New Service', 'elektrotehnika'),
			'view_item'				=> __('View Service', 'elektrotehnika')
		);

		$services_args = array(
			'label'               => __('Objects', 'elektrotehnika'),
			'description'         => __('Object information page', 'elektrotehnika'),
			'labels'              => $services_labels,
			'supports'            => array( 'title', 'thumbnail'),
			'taxonomies'          => array( 'objects-categories' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'has_archive'         => true,
			'can_export'          => true,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'query_var'           => true,
			'rewrite'             => '',
			'capability_type'     => 'post',
			'menu_position'		  => 4,
			'menu_icon'           => 'dashicons-hammer'
		);
		register_post_type( 'service', $services_args );
	}

	public function custom_taxonomy(){
		$taxonomy_labels = array(
			'name'                        => __('Services categories','elektrotehnika'),
			'singular_name'               => __('Service category','elektrotehnika'),
			'menu_name'                   => __('Services categories','elektrotehnika'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'service-category',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'service-category', 'service', $taxonomy_args );
	}

	public function service_block($id){
		$term = get_term($id, 'service-category');

		$background = ( get_field( 'thumbnail', 'service-category_'.$id ) ) ? ' style="background-image: url('.get_field( 'thumbnail', 'service-category_'.$id ).');"' : '';
		$output .= '<a class="service" data-aos="fade-up" data-aos-duration="1000" href="'.get_term_link($term->term_id, 'service-category').'"'.$background.'>';
			$output .= '<h4><b>'.$term->name.'</b></h4>';
		$output .= '</a>';

		return $output;
	}

	public function object_block($id){
		$output = '';
		$output .= '<a class="object" href="'.get_the_permalink($id).'" data-aos="fade-up" data-aos-duration="1000">';
			$output .= '<h4><b>'.get_the_title($id).'</b></h4>';
			if( get_the_excerpt($id) ) { 
				$output .= '<p>'.get_the_excerpt($id).'</p>';
			}

			$output .= '<div class="thumbnail">'.get_the_post_thumbnail($id, 'object-thumbnail').'</div>';
		$output .= '</a>';

		return $output;
	}

	public function load_object_block($page){
		$output = '<div class="col-lg-6">';
			$output .= '<a class="object" href="'.get_the_permalink($id).'">';
				$output .= '<h4><b>'.get_the_title($id).'</b></h4>';
				if( get_the_excerpt($id) ) { 
					$output .= '<p>'.get_the_excerpt($id).'</p>';
				}

				$output .= '<div class="thumbnail">'.get_the_post_thumbnail($id, 'object-thumbnail').'</div>';
			$output .= '</a>';
		$output .= '</div>';

		return $output;
	}

	public function load_objects(){
		$page = (int) $_POST['page'];

		$args = array(
			'posts_per_page'	=> 6,
			'post_type' 		=> 'object',
			'post_status'		=> 'publish',
			'paged'				=> $page
		);
		$query = new WP_Query( $args );
		$output = '';

		if ( $query->have_posts() ) :
			while ( $query->have_posts() ) { $query->the_post();
				$output .= $this->load_object_block( get_the_ID() );
			}
		endif;

		wp_reset_postdata();

		$return = array(
			'html'   => $output
		);

		wp_send_json_success($return);
	}

    public function __return_false() {
        return false;
    }
}

$elektrotehnika_xxi = new Elektrotehnika();