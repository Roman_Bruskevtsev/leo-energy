<section class="about__company">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<?php if( get_sub_field('left_side') ) { ?>
				<div class="left__side" data-aos="fade-left" data-aos-duration="1000"><?php the_sub_field('left_side'); ?></div>
				<?php } ?>
			</div>
			<div class="col-lg-8">
				<?php if( get_sub_field('right_side') ) { ?>
				<div class="right__side" data-aos="fade-right" data-aos-duration="1000"><?php the_sub_field('right_side'); ?></div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>