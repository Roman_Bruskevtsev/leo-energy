<?php
$background = get_sub_field('background') ? ' style="background-image: url('.get_sub_field('background')['url'].');"' : '';
?>
<section class="contact__us"<?php echo $background; ?>>
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
			<?php if( get_sub_field('title') ) { ?>
				<div class="title" data-aos="fade-left" data-aos-duration="1000"><h2><?php the_sub_field('title'); ?></h2></div>
			<?php } ?>
			<?php if( get_sub_field('text') ) { ?>
				<div class="text" data-aos="fade-up" data-aos-duration="1000"><?php the_sub_field('text'); ?></div>
			<?php } ?>
			<?php if( get_sub_field('form_shortcode') ) { ?>
				<div class="form__row" data-aos="fade-up" data-aos-duration="1000">
					<?php echo do_shortcode(get_sub_field('form_shortcode')); ?>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
</section>