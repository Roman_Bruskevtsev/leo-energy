<?php 
$elektrotehnika_xxi = new Elektrotehnika();
$services = get_sub_field('services');
?>
<section class="services">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php if( get_sub_field('title') ) { ?>
				<div class="title" data-aos="fade-right" data-aos-duration="1000">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
				<?php } ?>
			</div>
			<?php foreach ($services as $service) { ?>
				<div class="col-md-6 col-lg-3"><?php echo $elektrotehnika_xxi->service_block($service); ?></div>
			<?php } ?>
		</div>
	</div>
</section>