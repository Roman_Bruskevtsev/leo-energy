<section class="about__us">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<?php if( get_sub_field('title') ) { ?>
				<div class="title" data-aos="fade-left" data-aos-duration="1000">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
				<?php }
				if( get_sub_field('text') ) { ?>
				<div class="text" data-aos="fade-up" data-aos-duration="1000"><?php the_sub_field('text'); ?></div>
				<?php } 
				if( get_sub_field('link') ) { 
					$target = get_sub_field('link')['target'] ? ' target="'.get_sub_field('link')['target'].'"' : ''; ?>
					<a class="btn btn__black" href="<?php echo get_sub_field('link')['url']; ?>"<?php echo $target; ?> data-aos="fade-up" data-aos-duration="1000"><?php echo get_sub_field('link')['title']; ?></a>
				<?php } ?>
			</div>
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-8">
						<?php if( get_sub_field('background') ) { ?>
						<div data-aos="fade-right" data-aos-duration="1000" class="background" style="background-image: url('<?php echo get_sub_field('background')['url']; ?>');"></div>
						<?php } ?>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4"></div>
					<div class="col-lg-8">
						<?php 
						$slider = get_sub_field('slider');
						if( $slider ) { ?>
						<div class="slider" data-aos="fade-left" data-aos-duration="1000">
						<?php foreach ($slider as $slide) { ?>
							<div class="slide" style="background-image: url(<?php echo $slide['url']; ?>);"></div>
						<?php } ?>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>