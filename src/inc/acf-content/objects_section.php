<?php
$objects = get_sub_field('objects');
$elektrotehnika_xxi = new Elektrotehnika();
?><section class="objects__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="title" data-aos="fade-left" data-aos-duration="1000">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php } 
		if( $objects ) { ?>
		<div class="row">
			<?php foreach ( $objects as $object ) { ?>
			<div class="col-lg-6">
				<?php echo $elektrotehnika_xxi->object_block( $object->ID ); ?>
			</div>
			<?php } ?>
		</div>
		<?php } 
		if( get_sub_field('link') ) { 
			$target = get_sub_field('link')['target'] ? ' target="'.get_sub_field('link')['target'].'"' : '';
		?>
		<div class="row">
			<div class="col-lg-12">
				<div class="btn__row text-center">
					<a class="btn btn__black" href="<?php echo get_sub_field('link')['url']; ?>"<?php echo $target; ?> data-aos="fade-up" data-aos-duration="1000"><?php echo get_sub_field('link')['title']; ?></a>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>