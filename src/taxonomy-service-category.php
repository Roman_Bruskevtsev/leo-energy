<?php
/**
 *
 * @package WordPress
 * @subpackage Leo-energy
 * @since 1.0
 * @version 1.0
 */
get_header();

get_template_part( 'template-parts/service/content-term', 'banner' );
get_template_part( 'template-parts/page/breadcrumb' );

if ( have_posts() ) : ?>
<section class="services__section">
	<div class="container">
		<div class="row">
		<?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'template-parts/service/content', 'block'); ?>
        <?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; 

get_template_part( 'template-parts/service/content', 'consultation' );
get_template_part( 'template-parts/service/content', 'objects' ); 

get_footer();