<?php
/**
 *
 * @package WordPress
 * @subpackage Leo-energy
 * @since 1.0
 * @version 1.0
 */
?>  
    </main>
    <footer>
    	<div class="top__line">
    		<div class="container">
	    		<div class="row">
	    			<div class="col-lg-12">
	    				<?php 
                        $logo = get_field('footer_logo', 'option');
                        if( $logo ) { ?>
                        <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                        </a>
                        <?php } 
                        if( has_nav_menu('footer') ) { ?>
                        <div class="menu__block float-right">
                            <?php wp_nav_menu( array(
                                'theme_location'        => 'footer',
                                'container'             => 'nav',
                                'container_class'       => 'footer__nav'
                            ) ); ?>
                        </div>
                        <?php } ?>
	    			</div>
	    		</div>
	    	</div>
    	</div>
    	<div class="bottom__line">
    		<div class="container">
	    		<div class="row">
	    			<div class="col-lg-12">
	    				<?php if( get_field('copyright_text', 'option') ) { ?>
	    					<div class="copyright float-left">
	    						<p><?php the_field('copyright_text', 'option'); ?></p>
	    					</div>
	    				<?php } 
	    				if( get_field('social_links_title', 'option') ||
	    					get_field('instagram_link', 'option') || 
	    					get_field('facebook_link', 'option') ) { ?>
	    					<div class="social__block float-right">
	    						<?php if( get_field('social_links_title', 'option') ) { ?><h6><?php the_field('social_links_title', 'option'); ?></h6><?php } ?>
	    						<?php if( get_field('instagram_link', 'option') ) { ?><a class="instagram" href="<?php the_field('instagram_link', 'option'); ?>" target="_blank"></a><?php } ?>
	    						<?php if( get_field('facebook_link', 'option') ) { ?><a class="facebook" href="<?php the_field('facebook_link', 'option'); ?>" target="_blank"></a><?php } ?>
	    					</div>
	    				<?php } ?>
	    			</div>
	    		</div>
	    	</div>
    	</div>
    	
    </footer>
    <?php wp_footer(); ?>
</body>
</html>