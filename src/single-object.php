<?php
/**
 *
 * @package WordPress
 * @subpackage Leo-energy
 * @since 1.0
 * @version 1.0
 */
get_header(); 

get_template_part( 'template-parts/object/content', 'banner' );
get_template_part( 'template-parts/page/breadcrumb' );
get_template_part( 'template-parts/object/content', 'gallery' );
get_template_part( 'template-parts/service/content', 'consultation' );

get_footer();