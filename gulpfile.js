'use strict';
var themeName   = 'leo-energy',
    gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    sass        = require('gulp-sass'),
    cssmin      = require('gulp-cssmin'),
    babel       = require('gulp-babel'),
    jsmin       = require('gulp-uglify'),
    rename      = require('gulp-rename'),
    pipeline    = require('readable-stream').pipeline,
    buildFolder = 'build/wp-content/themes/' + themeName,
    buildPlugin = 'build/plugins';

gulp.task('wordpress-style', function () {
    return gulp.src('src/style.css')
        .pipe(gulp.dest(buildFolder));
});

gulp.task('wordpress-screen', function () {
    return gulp.src('src/screenshot.png')
        .pipe(gulp.dest(buildFolder));
});

gulp.task('styles',  function () {
    return gulp.src('src/assets/sass/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest(buildFolder + '/assets/css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(buildFolder + '/assets/css/'));    
});

gulp.task('scripts', function () {
    return gulp.src('src/assets/js/*.js')
           // .pipe(babel({
           //      presets: ['@babel/preset-env']
           //  }))
           // .pipe(jsmin())
           .pipe(rename({suffix: '.min'}))
           .pipe(gulp.dest(buildFolder + '/assets/js/'));
});

gulp.task('class-js', function () {
    return gulp.src('src/inc/classes/assets/js/**/*.js')
           .pipe(rename({suffix: '.min'}))
           .pipe(gulp.dest(buildFolder + '/inc/classes/assets/js/'));
});

gulp.task('php', function () {
    return gulp.src('src/**/**/*.php')
        .pipe(gulp.dest(buildFolder));
});

gulp.task('images', function() {
    return gulp.src('src/assets/images/**/*')
        .pipe(gulp.dest(buildFolder + '/assets/images'));
});

gulp.task('fonts', function() {
    return gulp.src('src/assets/fonts/**/*')
        .pipe(gulp.dest(buildFolder + '/assets/fonts'));
});

gulp.task('default', gulp.series(
    gulp.parallel(
    'wordpress-style',
    'wordpress-screen',
    'styles',
    'scripts',
    'class-js',
    'php',
    'images',
    'fonts'
    )
));

gulp.task('watch', function () {
    gulp.watch('src/assets/sass/**/*.scss', gulp.series('styles'));

    gulp.watch('src/assets/js/**/*.js', gulp.series('scripts'));

    gulp.watch('src/inc/classes/assets/js/**/*.js', gulp.series('class-js'));

    gulp.watch('src/**/*.php', gulp.series('php'));

    gulp.watch('src/assets/images/**/*', gulp.series('images'));
});